#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/xattr.h>
#include <sys/wait.h>
#include <pthread.h>

#define LOG_FILE_PATH "/home/fred/fs_module.log" // Ganti [user] dengan username yang sesuai
static const char *dirPath = "/home/fred/Downloads";
char *mod = "module_";

void splitFile(const char *filePath, const char *logPath, int blockSize)
{
    FILE *originalFile = fopen(filePath, "rb");
    if (originalFile == NULL)
    {
        perror("Error opening original file");
        exit(EXIT_FAILURE);
    }

    fseek(originalFile, 0, SEEK_END);
    long fileSize = ftell(originalFile);
    if (fileSize <= 1024)
        return;
    rewind(originalFile);

    int numBlocks = fileSize / blockSize;
    int lastBlockSize = fileSize % blockSize;

    char buffer[blockSize];

    for (int i = 1; i <= numBlocks; i++)
    {
        char newFilePath[2000];
        snprintf(newFilePath, sizeof(newFilePath), "%s.%03d", filePath, i);

        FILE *newFile = fopen(newFilePath, "wb");
        if (newFile == NULL)
        {
            perror("Error creating new file");
            exit(EXIT_FAILURE);
        }

        fread(buffer, sizeof(char), blockSize, originalFile);
        fwrite(buffer, sizeof(char), blockSize, newFile);
        fclose(newFile);
    }

    if (lastBlockSize > 0)
    {
        char newFilePath[2000];
        snprintf(newFilePath, sizeof(newFilePath), "%s.%03d", filePath, numBlocks);

        FILE *newFile = fopen(newFilePath, "wb");
        if (newFile == NULL)
        {
            perror("Error creating new file");
            exit(EXIT_FAILURE);
        }

        fread(buffer, sizeof(char), lastBlockSize, originalFile);
        fwrite(buffer, sizeof(char), lastBlockSize, newFile);
        fclose(newFile);
    }

    fclose(originalFile);
    remove(filePath);
}

// Fungsi untuk menggabungkan file-file kecil menjadi satu file utuh
void mergeFiles(const char *filePath)
{
    FILE *output_file;
    FILE *input_file;
    char buffer[1024];
    int part_number = 1;
    char part_filename[5000];
    char full_filename[4000];
    sprintf(full_filename, "%s", filePath);
    output_file = fopen(full_filename, "wb");

    if (output_file == NULL)
    {
        return;
    }
    while (1)
    {
        sprintf(part_filename, "%s.%03d", full_filename, part_number);

        input_file = fopen(part_filename, "rb");
        if (input_file == NULL)
        {
            break;
        }

        while (fread(buffer, sizeof(char), sizeof(buffer), input_file) > 0)
        {
            fwrite(buffer, sizeof(char), sizeof(buffer), output_file);
        }

        fclose(input_file);

        char cmd[6000];
        sprintf(cmd, "rm %s", part_filename);
        system(cmd);
        remove(part_filename);

        part_number++;
    }

    // Menutup file output
    fclose(output_file);

    return;
}

// Fungsi untuk melakukan modularisasi pada direktori dan kontennya
void modularizeDirectory(const char *directoryPath, const char *logPath)
{
    DIR *directory = opendir(directoryPath);
    if (directory == NULL)
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(directory)) != NULL)
    {
        if (entry->d_type == DT_DIR && strncmp(entry->d_name, "module_", 7) == 0)
        {
            char subDirectoryPath[1000];
            sprintf(subDirectoryPath, "%s/%s", directoryPath, entry->d_name);

            modularizeDirectory(subDirectoryPath, logPath);
        }
        else if (entry->d_type == DT_REG)
        {
            char filePath[1000];
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);

            splitFile(filePath, logPath, 1024);
        }
    }
    remove(directoryPath);
    closedir(directory);
}

void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat statbuf;

    char prevpath[4000];
    char currpath[4000];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            snprintf(path, sizeof(path), "%s/%s", basePath, dp->d_name);

            if (stat(path, &statbuf) == 0)
            {
                if (S_ISDIR(statbuf.st_mode))
                {
                    listFilesRecursively(path);
                }
                else
                {
                    char *substring;
                    int substring_length;
                    const char *dot = strchr(path, '.');
                    int length = strlen(path);
                    int dotCount = 0;
                    int i_loop;

                    for (i_loop = 0; i_loop < length; i_loop++)
                    {
                        if (path[i_loop] == '.')
                        {
                            dotCount++;
                            if (dotCount == 2)
                            {
                                break;
                            }
                        }
                    }

                    if (dotCount == 2)
                    {
                        substring_length = (int)(dot - path);
                        substring = (char *)malloc(substring_length + 1);

                        strncpy(substring, path, i_loop);
                        substring[i_loop] = '\0';
                        sprintf(currpath, "%s", substring);
                        free(substring);

                        if (strstr(prevpath, currpath) == NULL)
                        {
                            printf("Currpath : %s", currpath);
                            mergeFiles(currpath);
                            dotCount = 0;
                        }
                    }
                    printf("\n");
                }

                sprintf(prevpath, "%s", currpath);
            }
        }
    }
}

// Fungsi untuk mengembalikan direktori menjadi tidak modular
void restoreDirectory(const char *directoryPath)
{
    DIR *directory = opendir(directoryPath);
    if (directory == NULL)
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(directory)) != NULL)
    {
        if (entry->d_type == DT_DIR && strncmp(entry->d_name, "module_", 7) == 0)
        {
            char subDirectoryPath[1000];
            sprintf(subDirectoryPath, "%s/%s", directoryPath, entry->d_name);

            restoreDirectory(subDirectoryPath);
        }
        else if (entry->d_type == DT_REG && strstr(entry->d_name, ".") != NULL)
        {
            char filePath[1000];
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);

            remove(filePath);
        }
    }

    closedir(directory);
}

enum LogLevel
{
    REPORT,
    FLAG
};

void logSystemCall(enum LogLevel level, const char *cmd, const char *desc)
{
    FILE *logFile = fopen(LOG_FILE_PATH, "a");
    if (logFile == NULL)
    {
        perror("Error opening log file");
        return;
    }

    time_t rawTime;
    struct tm *timeInfo;
    char timeStr[20];
    time(&rawTime);
    timeInfo = localtime(&rawTime);
    strftime(timeStr, sizeof(timeStr), "%y%m%d-%H:%M:%S", timeInfo);

    const char *levelStr = (level == REPORT) ? "REPORT" : "FLAG";

    fprintf(logFile, "%s::%s::%s::%s\n", levelStr, timeStr, cmd, desc);
    fclose(logFile);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = lstat(filepath, stbuf);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "GETATTR", filepath);
    return 0;
}

static int xmp_access(const char *path, int mask)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = access(filepath, mask);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "ACCESS", filepath);
    return 0;
}

static int xmp_readlink(const char *pathname, char *data, size_t data_size)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, pathname);
    res = readlink(filepath, data, data_size);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "READLINK", filepath);
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi)
{
    char *a = strstr(path, mod);

    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    dp = opendir(filepath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);

    if (a != NULL)
        modularizeDirectory(dirPath, LOG_FILE_PATH);
    logSystemCall(REPORT, "READDIR", filepath);
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = mknod(filepath, mode, rdev);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "ACCESS", filepath);
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = mkdir(filepath, mode);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "MKDIR", filepath);
    return 0;
}

static int xmp_symlink(const char *target, const char *linkname)
{
    int res;

    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, target);
    sprintf(filepath2, "%s%s", dirPath, linkname);
    res = symlink(filepath1, filepath2);
    if (res == -1)
        return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);
    logSystemCall(REPORT, "SYMLINK", filetotal);
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = unlink(filepath);
    if (res == -1)
        return -errno;

    logSystemCall(FLAG, "UNLINK", filepath);
    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = rmdir(filepath);
    if (res == -1)
        return -errno;

    logSystemCall(FLAG, "RMDIR", filepath);
    return 0;
}

// !!!!!!!!!!!! MODIF
static int xmp_rename(const char *from, const char *to) // belum di modifikasi
{
    int res;

    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, from);
    sprintf(filepath2, "%s%s", dirPath, to);
    res = rename(filepath1, filepath2);
    if (res == -1)
        return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);

    char *b = strstr(to, mod);

    if (b == NULL)
        listFilesRecursively(filepath2);
    if (b != NULL)
        modularizeDirectory(filepath2, LOG_FILE_PATH);
    logSystemCall(REPORT, "RENAME", filetotal);
    return 0;
}

static int xmp_link(const char *oldpath, const char *newpath)
{
    int res;
    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, oldpath);
    sprintf(filepath2, "%s%s", dirPath, newpath);
    res = link(filepath1, filepath2);
    if (res == -1)
        return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);
    logSystemCall(REPORT, "LINK", filetotal);
    return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = chmod(filepath, mode);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "CHMOD", filepath);
    return 0;
}

static int xmp_chown(const char *path, uid_t owner, gid_t group)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = chown(filepath, owner, group);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "CHOWN", filepath);
    return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = truncate(path, size);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "TRUNCATE", path);
    return 0;
}

static int xmp_utimens(const char *path, const struct timespec times[2])
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);

    struct timeval tv[2];
    tv[0].tv_sec = times[0].tv_sec;
    tv[0].tv_usec = times[1].tv_nsec / 1000;
    tv[1].tv_sec = times[1].tv_sec;
    tv[1].tv_usec = times[1].tv_nsec / 1000;

    res = utimes(path, tv);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "UTIMENS", filepath);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = open(filepath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    logSystemCall(REPORT, "OPEN", filepath);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi)
{
    int fd;
    int res;

    (void)fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    fd = open(filepath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    logSystemCall(REPORT, "READ", filepath);
    return res;
}

static int xmp_write(const char *path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;

    (void)fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    fd = open(filepath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    logSystemCall(REPORT, "WRITE", filepath);
    return res;
}

static int xmp_statfs(const char *pathname, struct statvfs *data)
{
    int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, pathname);
    res = statvfs(filepath, data);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "STAFFTS", filepath);
    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);

    (void)fi;
    int res = creat(filepath, mode);
    if (res == -1)
    {
        return -errno;
    }
    close(res);

    logSystemCall(REPORT, "CREATE", path);
    return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
    int res;

    int fd = fi->fh;

    res = close(fd);
    if (res == -1)
    {
        return -errno;
    }
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    logSystemCall(REPORT, "RELEASE", filepath);
    return 0;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi)
{
    int fd = fi->fh;
    int res = isdatasync ? fdatasync(fd) : fsync(fd);

    if (res == -1)
    {
        return -errno;
    }
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    logSystemCall(REPORT, "FSYNC", filepath);
    return 0;
}

static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
    int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = setxattr(filepath, name, value, size, flags);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "SETXATTR", filepath);
    return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value, size_t size)
{
    ssize_t res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = getxattr(filepath, name, value, size);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "GETXATTR", filepath);
    return res;
}

static int xmp_listxattr(const char *path, char *list, size_t size)
{
    int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = listxattr(filepath, list, size);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "LISTXATTR", filepath);
    return 0;
}

static int xmp_removexattr(const char *path, const char *list)
{
    int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);
    res = removexattr(filepath, list);
    if (res == -1)
        return -errno;

    logSystemCall(REPORT, "TRUNCATE", filepath);
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}