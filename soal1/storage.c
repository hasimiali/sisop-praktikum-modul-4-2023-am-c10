#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1000
#define MAX_FIELD_LENGTH 100

int main() {

    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");

    FILE *file;
    char line[MAX_LINE_LENGTH];
    char *field;
    char fields[MAX_FIELD_LENGTH][MAX_FIELD_LENGTH];
    int num_fields, i;
    int first = 1;

    // Buka berkas CSV
    file = fopen("FIFA23_official_data.csv", "r"); // Ganti dengan jalur berkas CSV yang sebenarnya

    if (file == NULL) {
        printf("Gagal membuka berkas.\n");
        return 1;
    }

    // Baca baris per baris
    while (fgets(line, sizeof(line), file)) {
        num_fields = 0;

        // Pisahkan baris menjadi field menggunakan pemisah koma
        field = strtok(line, ",");

        while (field != NULL) {
            strcpy(fields[num_fields], field);
            num_fields++;

            // Baca field berikutnya
            field = strtok(NULL, ",");
        }
        if (first) {
            first = 0;
            continue;
        }
        int age = 0, potent = 0;
        age = atoi(fields[2]);
        potent = atoi(fields[7]);

        if (age > 25 || potent < 85 || !strcmp(fields[8], "Manchester City")) continue;
        // Lakukan pengolahan data per baris di sini 
        for (i = 0; i < num_fields; i++) {//nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.

            if (i == 1 || i == 2 || i == 3
                || i == 4 || i == 7 || i == 8) {  //Nama, Foto, Umur, Potential, nationality, club
                if (i == 1) printf("Name: ");
                if (i == 2) printf("Age: ");
                if (i == 3) printf("Photo: ");
                if (i == 4) printf("Nationality: ");
                if (i == 7) printf("Potential: ");
                if (i == 8) printf("Club: ");
                printf("%s\n", fields[i]);
            }
        }
        printf("\n");
    }
    // Tutup berkas CSV
    fclose(file);
    return 0;
}
