# Sistem Operasi Modul 4

KELOMPOK C10
## Anggota Kelompok

| NRP        | Nama                    |
|:----------:|:-----------------------:|
| 5025211131 | Ali Hasyimi Assegaf     |
| 5025211152 | Frederick Hidayat       |

# Soal 1
## A. 
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

### Jawab

```c
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
system("unzip fifa-player-stats-database.zip");

```
Dari kode diatas, akan didapatkan dataset terkait yang mana harus ada instalasi terlbih dahulu dari python dan kaggle supaya dapat dilakukan pendownloadan.

## B
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

### Jawab
```c
file = fopen("FIFA23_official_data.csv", "r"); // Ganti dengan jalur berkas CSV yang sebenarnya

    if (file == NULL) {
        printf("Gagal membuka berkas.\n");
        return 1;
    }

    // Baca baris per baris
    while (fgets(line, sizeof(line), file)) {
        num_fields = 0;

        // Pisahkan baris menjadi field menggunakan pemisah koma
        field = strtok(line, ",");

        while (field != NULL) {
            strcpy(fields[num_fields], field);
            num_fields++;

            // Baca field berikutnya
            field = strtok(NULL, ",");
        }
        if (first) {
            first = 0;
            continue;
        }
        int age = 0, potent = 0;
        age = atoi(fields[2]);
        potent = atoi(fields[7]);

        if (age > 25 || potent < 85 || !strcmp(fields[8], "Manchester City")) continue;
        // Lakukan pengolahan data per baris di sini 
        for (i = 0; i < num_fields; i++) {//nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.

            if (i == 1 || i == 2 || i == 3
                || i == 4 || i == 7 || i == 8) {  //Nama, Foto, Umur, Potential, nationality, club
                if (i == 1) printf("Name: ");
                if (i == 2) printf("Age: ");
                if (i == 3) printf("Photo: ");
                if (i == 4) printf("Nationality: ");
                if (i == 7) printf("Potential: ");
                if (i == 8) printf("Club: ");
                printf("%s\n", fields[i]);
            }
        }
        printf("\n");
    }
    // Tutup berkas CSV
    fclose(file);
```
Pada kode diatas, akan dibuka file terkait yang didapatkan dari hasil pengunduhan file terkait. Setelah file dibuka, pada `fields` akan diisi bagian-bagian dari satu row data yang nantinya pada bagian bawah akan dapat ditampilkan sesuai yang diinginkan oleh soal melalui pengaksesan indeks dari `fields`

## C dan D
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

### Jawab
```
FROM ubuntu:latest
RUN apt update && apt install python3 -y && apt install python3-pip -y
RUN pip install kaggle  
RUN apt install gcc -y
RUN apt install unzip -y
COPY kaggle.json /root/.kaggle/kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json
COPY storage.c /app/
COPY kaggle.json ~/.kaggle/kaggle.json
WORKDIR /app/
RUN gcc storage.c -o storage
```
Untuk melakukan upload ke docker hub, perlu kita siapkan dockerfile yang nantinya akan kita build dan dockerimage nya akan kita jalankan docker containernya kemudian jika semua sudah berhasil, dengan bantuan `docker push`, docker image kita akan diupload ke docker hub dan dapat diakses oleh semua orang selama aksesnya masih dibuka. 
```
https://hub.docker.com/r/mrhermes/storage-app/tags
```
Berikut link menuju docker image yang kami upload.

## E
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

### Jawab
```
version: "3"
services:
  barcelona: 
    build:
      context: ./Barcelona
    deploy:
      replicas: 5 
    command: tail -F anything
  napoli:
    build:
      context: ./Napoli
    deploy:
      replicas: 5
    command: tail -F anything
```
Dalam melakukan compose pada docker, dibutuhkan file konfigurasi yang sesuai dengan permintaan soal. Pada `replocas` diberikan value 5 supaya dapat terbuat 5 instance. Setelah itu, supaya container bisa berjalan dan tidak langsung keluar, diberikan `command: tail -F anything` supaya container bisa tetap berjalan.
Sebelum menjalankan docker-compose, dibuat folder Barcelona dan Napoli.

# Soal 3
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.
## A.
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 
```c
static const char *dirpath = "/home/cimi/Documents/Workspace/gitlab/sisop";

int main(int argc, char *argv[])
{
    umask(0);
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}
```

## B.
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
```c

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}
```

## C.
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
```c
        if (de->d_type == DT_REG)
        {
            if (is_encoded(name))
            {
                encodeBase64File(cur_path);
            }
            else if (strlen(name) <= 4)
            {
                convert_to_binary(name);
            }
            else
            {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else
        {
            if (strlen(name) <= 4)
            {
                convert_to_binary(name);
            }
            else
            {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
```

## D.
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
```c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
    }
    else
    {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0)
        {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0)
            {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```

## E.
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
 - Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
 - Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 

```c
version: "3"

services:
    dhafin:
        image: ubuntu:bionic
        container_name: Dhafin
        command: tail -f /dev/null
        volumes:
            - /home/cimi/Documents/Workspace/gitlab/sisop-praktikum-modul-4-2023-am-c10/testDir:/dhafin

    normal:
        image: ubuntu:bionic
        container_name: Normal
        command: tail -f /dev/null
        volumes:
            - /home/cimi/Documents/Workspace/gitlab/etc:/normal

```
# Soal 4
## FUSE Operation
Berikut FUSE Operation yang diminta oleh soal
```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};
```
Berikut isi tiap FUSE Operation
```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = lstat(filepath, stbuf);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "GETATTR", filepath);
    return 0;
}

static int xmp_access(const char* path, int mask){
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = access(filepath, mask);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "ACCESS", filepath);
    return 0;
}

static int xmp_readlink(const char *pathname, char *data, size_t data_size){
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, pathname );
	res = readlink(filepath, data, data_size);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "READLINK", filepath);
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = mknod(filepath, mode, rdev);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "ACCESS", filepath);
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = mkdir(filepath, mode);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "MKDIR", filepath);
    return 0;
}

static int xmp_symlink(const char *target, const char *linkname){
	int res;

    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, target);
    sprintf(filepath2, "%s%s", dirPath, linkname);
	res = symlink(filepath1, filepath2);
	if (res == -1)
		return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);
	logSystemCall( REPORT, "SYMLINK", filetotal);
    return 0;
}

static int xmp_unlink(const char *path)
{
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = unlink(filepath);
	if (res == -1)
		return -errno;

	logSystemCall( FLAG, "UNLINK", filepath);
    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = rmdir(filepath);
	if (res == -1)
		return -errno;

	logSystemCall( FLAG, "RMDIR", filepath);
    return 0;
}

static int xmp_link(const char *oldpath, const char *newpath){
	int res;
    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, oldpath);
    sprintf(filepath2, "%s%s", dirPath, newpath);
	res = link(filepath1, filepath2);
	if (res == -1)
		return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);
	logSystemCall( REPORT, "LINK", filetotal);
    return 0;
}

static int xmp_chmod(const char *path, mode_t mode){
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = chmod(filepath, mode);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "CHMOD", filepath);
    return 0;
}

static int xmp_chown(const char *path, uid_t owner, gid_t group){
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = chown(filepath, owner, group);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "CHOWN", filepath);
    return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
    int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = truncate(path, size);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "TRUNCATE", path);
    return 0;
}

static int xmp_utimens(const char* path, const struct timespec times[2]){
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path);

    struct timeval tv[2];
    tv[0].tv_sec = times[0].tv_sec;
    tv[0].tv_usec= times[1].tv_nsec/1000;
    tv[1].tv_sec = times[1].tv_sec;
    tv[1].tv_usec = times[1].tv_nsec/1000;

	res = utimes(path, tv);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "UTIMENS", filepath);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
	int res;

    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = open(filepath, fi->flags);
	if (res == -1)
		return -errno;

	close(res);
	logSystemCall( REPORT, "OPEN", filepath);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
		    struct fuse_file_info *fi)
{
	int fd;
	int res;

	(void) fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	fd = open(filepath, O_RDONLY);
	if (fd == -1)
		return -errno;

	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	logSystemCall( REPORT, "READ", filepath);
	return res;
}

static int xmp_write(const char *path, const char *buf, size_t size,
		     off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res;

	(void) fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	fd = open(filepath, O_WRONLY);
	if (fd == -1)
		return -errno;

	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	logSystemCall( REPORT, "WRITE", filepath);
	return res;
}

static int xmp_statfs(const char *pathname, struct statvfs *data){
	int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, pathname );
	res = statvfs(filepath, data);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "STAFFTS", filepath);
    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
    
    (void)fi;
    int res = creat(filepath, mode);
	if (res == -1){
        return -errno;
    }
    close(res);

	logSystemCall( REPORT, "CREATE", path);
    return 0;
}

static int xmp_release(const char* path, struct fuse_file_info *fi){
	int res;

    int fd = fi->fh;

	res = close(fd);
    if(res == -1){
        return -errno;
    }
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	logSystemCall( REPORT, "RELEASE", filepath);
    return 0;
}

static int xmp_fsync(const char* path, int isdatasync, struct fuse_file_info* fi){
	int fd = fi->fh;
    int res = isdatasync ? fdatasync(fd) : fsync(fd);

	if(res == -1){
        return -errno;
    }
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	logSystemCall( REPORT, "FSYNC", filepath);
    return 0;
}

static int xmp_setxattr(const char* path, const char* name, const char* value, size_t size, int flags){
	int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = setxattr(filepath, name, value, size, flags);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "SETXATTR", filepath);
    return 0;
}

static int xmp_getxattr(const char* path, const char* name, char* value, size_t size){
	ssize_t res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res =getxattr(filepath, name, value, size);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "GETXATTR", filepath);
    return res;
}

static int xmp_listxattr(const char* path, char* list, size_t size){
	int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = listxattr(filepath, list, size);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "LISTXATTR", filepath);
    return 0;
}

static int xmp_removexattr(const char *path, const char *list){
	int res;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	res = removexattr(filepath, list);
	if (res == -1)
		return -errno;

	logSystemCall( REPORT, "TRUNCATE", filepath);
    return 0;
}
```
Pada FUSE operation di atas, dilakukan penambahan untuk pelaporan log saja, sehingga tidak ada perubahan operasi yang dilakukan oleh kode di atas.
Berikut fungsi untuk melakukan log report, sebelumnya pada tiap FUSE operation sudah diberikan tag REPORT maupun FLAG sesuai keinginan soal.
```c
void logSystemCall(enum LogLevel level, const char *cmd, const char *desc) {
    FILE *logFile = fopen(LOG_FILE_PATH, "a");
    if (logFile == NULL) {
        perror("Error opening log file");
        return;
    }

    time_t rawTime;
    struct tm *timeInfo;
    char timeStr[20];
    time(&rawTime);
    timeInfo = localtime(&rawTime);
    strftime(timeStr, sizeof(timeStr), "%y%m%d-%H:%M:%S", timeInfo);

    const char *levelStr = (level == REPORT) ? "REPORT" : "FLAG";

    fprintf(logFile, "%s::%s::%s::%s\n", levelStr, timeStr, cmd, desc);
    fclose(logFile);
}
```
Di bawah ini terdapat dua FUSE operation yang ditambahi kondisi-kondisi tertentu untuk menyesuaikan permintaan soal yaitu modularisasi file.
Pada `rename` akan dilakukan modularisasi jika sebuah directory di rename dengan nama yang mengandung `module_`, sedangkan jika direname menjadi nama lain selain `module_` maka modularisasi akan di _revert_.
Pada `readdir` akan dilakukan pembacaan directory dan jika namanya mengandung `module` maka akan dilakukan modularisasi.
Hal ini akan sesuai nantinya dengan keinginan dari soal yang mana jika dibayangkan, sebuah directory yang baru saja dibuat akan kosong isinya sehingga jika modularisasi ingin dilakukan, haruslah diletakkan pada readdir agar pada tiap pengaksesan directory dapat dilakukan modularisasi bukan hanya memodularisasi directory kosong.
```c
static int xmp_rename(const char *from, const char *to)
{
    int res;

    char filepath1[1000], filepath2[1000];
    sprintf(filepath1, "%s%s", dirPath, from);
    sprintf(filepath2, "%s%s", dirPath, to);
	res = rename(filepath1, filepath2);
	if (res == -1)
		return -errno;

    char filetotal[2100];
    sprintf(filetotal, "%s to %s", filepath1, filepath2);

    char *b = strstr(to, mod);

    if (b == NULL) listFilesRecursively(filepath2);
    if (b != NULL) modularizeDirectory(filepath2, LOG_FILE_PATH);
	logSystemCall( REPORT, "RENAME", filetotal);
    return 0;

}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		       off_t offset, struct fuse_file_info *fi)
{
    char *a = strstr(path, mod);

	DIR *dp;
	struct dirent *de;

	(void) offset;
	(void) fi;
    char filepath[1000];
    sprintf(filepath, "%s%s", dirPath, path );
	dp = opendir(filepath);
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		if (filler(buf, de->d_name, &st, 0))
            break;
	}

	closedir(dp);

    if (a!=NULL)
        modularizeDirectory(dirPath, LOG_FILE_PATH);
    logSystemCall( REPORT, "READDIR", filepath);
	return 0;
}
```

## Modularisasi
Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

### Jawab
Berikut fungsi-fungsi yang berperan dalam modularisasi
Untuk pemecahan fragment-fragment,
```c
void splitFile(const char* filePath, const char* logPath, int blockSize) {
    FILE* originalFile = fopen(filePath, "rb");
    if (originalFile == NULL) {
        perror("Error opening original file");
        exit(EXIT_FAILURE);
    }

    fseek(originalFile, 0, SEEK_END);
    long fileSize = ftell(originalFile);
    if (fileSize <= 1024) return;
    rewind(originalFile);

    int numBlocks = fileSize / blockSize;
    int lastBlockSize = fileSize % blockSize;

    char buffer[blockSize];

    for (int i = 1; i <= numBlocks; i++) {
        char newFilePath[2000];
        snprintf(newFilePath, sizeof(newFilePath), "%s.%03d", filePath, i);

        FILE* newFile = fopen(newFilePath, "wb");
        if (newFile == NULL) {
            perror("Error creating new file");
            exit(EXIT_FAILURE);
        }

        fread(buffer, sizeof(char), blockSize, originalFile);
        fwrite(buffer, sizeof(char), blockSize, newFile);
        fclose(newFile);
    }

    if (lastBlockSize > 0) {
        char newFilePath[2000];
        snprintf(newFilePath, sizeof(newFilePath), "%s.%03d", filePath, numBlocks);

        FILE* newFile = fopen(newFilePath, "wb");
        if (newFile == NULL) {
            perror("Error creating new file");
            exit(EXIT_FAILURE);
        }

        fread(buffer, sizeof(char), lastBlockSize, originalFile);
        fwrite(buffer, sizeof(char), lastBlockSize, newFile);
        fclose(newFile);
    }

    fclose(originalFile);
    remove(filePath);
}
```
Sebagai main function dan 
```c
void modularizeDirectory(const char* directoryPath, const char* logPath) {
    DIR* directory = opendir(directoryPath);
    if (directory == NULL) {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent* entry;
    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_DIR && strncmp(entry->d_name, "module_", 7) == 0) {
            char subDirectoryPath[1000];
            sprintf(subDirectoryPath, "%s/%s", directoryPath, entry->d_name);

            modularizeDirectory(subDirectoryPath, logPath);
        }
        else if (entry->d_type == DT_REG) {
            char filePath[1000];
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);

            splitFile(filePath, logPath, 1024);
        }
    }
    remove(directoryPath);
    closedir(directory);
}
```
sebagai utility function.
Pada kode diatas, kode bekerja dengan merekursi dan membaca tiap file kemudian akan memecah file menjadi fragment-fragment file yang sesuai dengan ketentuan yaitu 1024 KB maksimal. Jika file tidak lebih dari 1024 maka akan dilanjutkan ke file selanjutnya. Dengan bantuan `numBlocks`, fragment file akan dinamai sesuai urutannya yang mana `numBlocks` diambil dari membagi ukuran file dengan 1024.

Untuk menggabungkan fragment digunakan kode ini,
```c
void mergeFiles(const char* filePath) {
    FILE *output_file;
    FILE *input_file;
    char buffer[1024];
    int part_number = 1;
    char part_filename[5000];
    char full_filename[4000];
    sprintf(full_filename, "%s", filePath);
    output_file = fopen(full_filename, "wb");

    if (output_file == NULL)
    {
        return;
    }
    while (1)
    {
        sprintf(part_filename, "%s.%03d", full_filename, part_number);

        input_file = fopen(part_filename, "rb");
        if (input_file == NULL)
        {
            break;
        }

        while (fread(buffer, sizeof(char), sizeof(buffer), input_file) > 0)
        {
            fwrite(buffer, sizeof(char), sizeof(buffer), output_file);
        }

        fclose(input_file);

        char cmd[6000];
        sprintf(cmd, "rm %s", part_filename);
        system(cmd);
        remove(part_filename);

        part_number++;
    }

    // Menutup file output
    fclose(output_file);

    return;

}
```
sebagai main function dan kode di bawah sebagai utility function
```c
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat statbuf;

    char prevpath[4000];
    char currpath[4000];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            snprintf(path, sizeof(path), "%s/%s", basePath, dp->d_name);

            if (stat(path, &statbuf) == 0)
            {
                if (S_ISDIR(statbuf.st_mode))
                {
                    listFilesRecursively(path);
                }
                else
                {
                    char *substring;
                    int substring_length;
                    const char *dot = strchr(path, '.');
                    int length = strlen(path);
                    int dotCount = 0;
                    int i_loop;

                    for (i_loop = 0; i_loop < length; i_loop++)
                    {
                        if (path[i_loop] == '.')
                        {
                            dotCount++;
                            if (dotCount == 2)
                            {
                                break;
                            }
                        }
                    }

                    if (dotCount == 2)
                    {
                        substring_length = (int)(dot - path);
                        substring = (char *)malloc(substring_length + 1);

                        strncpy(substring, path, i_loop);
                        substring[i_loop] = '\0';
                        sprintf(currpath, "%s", substring);
                        free(substring);

                        if (strstr(prevpath, currpath) == NULL)
                        {
                            printf("Currpath : %s",currpath);
                            mergeFiles(currpath);
                            dotCount = 0;
                        }
                    }
                        printf("\n");
                }  

                sprintf(prevpath, "%s", currpath);
            }
        }
    }
}
```
Pada kode di atas, akan dilakukan pengidentifikasian nama asli dari file yang telah di modularisasi, kemudian merekursi satu per satu fragment file yang termodularisasi, kembali menjadi satu file utuh. Isi dari fragment file akan dimasukan melalui kode 
```c
while (fread(buffer, sizeof(char), sizeof(buffer), input_file) > 0)
        {
            fwrite(buffer, sizeof(char), sizeof(buffer), output_file);
        }
```
kemudian fragment file akan di remove.

### Lampiran No 4
Kutipan Log File Line 11708 - 11727
```
REPORT::230603-17:33:02::ACCESS::/home/fred/Downloads/testdir
REPORT::230603-17:33:02::ACCESS::/home/fred/Downloads/testdir
REPORT::230603-17:33:02::READDIR::/home/fred/Downloads/testdir
REPORT::230603-17:33:07::GETATTR::/home/fred/Downloads/
REPORT::230603-17:33:07::GETATTR::/home/fred/Downloads/testdir
FLAG::230603-17:33:07::RMDIR::/home/fred/Downloads/testdir
REPORT::230603-17:33:28::GETATTR::/home/fred/Downloads/
REPORT::230603-17:33:28::READDIR::/home/fred/Downloads/
REPORT::230603-17:33:28::GETATTR::/home/fred/Downloads/module_
REPORT::230603-17:33:28::READDIR::/home/fred/Downloads/module_
REPORT::230603-17:33:45::GETATTR::/home/fred/Downloads/
REPORT::230603-17:33:45::CREATE::/modular.c
REPORT::230603-17:33:45::GETATTR::/home/fred/Downloads/modular.c
REPORT::230603-17:33:45::ACCESS::/home/fred/Downloads/
REPORT::230603-17:33:45::GETATTR::/home/fred/Downloads/
FLAG::230603-17:33:45::UNLINK::/home/fred/Downloads/modular.c
REPORT::230603-17:33:45::GETATTR::/home/fred/Downloads/.Trash-1000
REPORT::230603-17:34:05::GETATTR::/home/fred/Downloads/module_
REPORT::230603-17:34:05::GETATTR::/home/fred/Downloads/
REPORT::230603-17:34:05::ACCESS::/home/fred/Downloads/module_
```
Melalui rename atau di mount pertama kali
Directory ketika namanya tidak mengandung `module_`
![WhatsApp Image 2023-06-03 at 19 01 40](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/9d662d2e-2fb5-4c78-a66a-015ca98c2982)

Directory ketika namanya mengandung `module_`

![WhatsApp Image 2023-06-03 at 19 02 18](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/dc13744e-0f2b-46f3-9863-68607210f3dd)



